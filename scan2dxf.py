# Imports
import sane
import time
import os
import sys
import shutil
import subprocess
from gpiozero import Button
from gpiozero import LED

Button.was_held = False

#On définit le bouton de prise de vue et le temps d'appui long à 5s
bouton=Button(17, hold_time=5)

#On définit les LEDs
OkLed = LED(27)
BusyLed = LED(22)

#On éteind les LEDs
OkLed.off()
BusyLed.off()

# On purge le répertoire /home/pi/DXF
os.system ('rm -rf /home/pi/Scan2dxf/DXF/*')
os.system ('rm -rf /home/pi/Scan2dxf/BMP/*')
os.system ('rm -rf /home/pi/Scan2dxf/PBM/*')
os.system ('rm -rf /home/pi/Scan2dxf/SVG/*')
os.system ('rm -rf /home/pi/Scan2dxf/PDF/*')

# On définit les réglages du scanner
scan_resolution = 300
scan_luminosite = 50
scan_contraste = 70

# Définition de la fonction extinction
def shutdown():
    subprocess.call(['sudo shutdown -h now "Arrêt du systeme demandé par appui long sur le bouton" &'], shell=True)

# Définition de la procedure de scan et traitement de l'image       
def procedure_scan():    
    OkLed.off()
    BusyLed.on()
    timestr = time.strftime("%H%M%S")
    # On définit les noms des différents fichiers qui seront utilisés
    fichier = "Scan_master_" +timestr
    fichier_bmp = fichier +".bmp" #résultat du scan au format bmp
    fichier_pbm = fichier +".pbm" #résultat du traitement de mkbitmap
    fichier_dxf = fichier +".dxf" #résultat de la vectorisation au format dxf r12
    fichier_svg = fichier +".svg" #résultat de la vectorisation au format svg
    fichier_pdf = fichier +".pdf" #résultat de la vectorisationcenterline  au format svg

    # On lance la numérisation
    print("Lancement du " +fichier +", numérisation en cours...")
    # Pour modele Canon : 
    img=myscanner.scan()
    img.save(fichier_bmp)
    print (fichier +" terminé, début du traitement de l\'image")
           
    # On lance le traitement bitmap pour éliminer les imperfections du scan
    os.system ('mkbitmap -n -s 2 -b 6 -t 0.7 ' +fichier_bmp)
    print("Traitement de l'image terminé, exportation des fichiers vectoriels...")
           
    # On effectue la vectorisation avec potrace
    os.system ('potrace ' +fichier_pbm +" -A 180 -H 11.693 -b dxf")
    os.system ('potrace ' +fichier_pbm +" -A 180 -H 11.693 -b svg")
    os.system ('potrace ' +fichier_pbm +" -A 180 -H 11.693 -b pdf")
    print ("Traçage de la forme terminé")
            
    # On déplace les fichiers vers le dossier partagé
    shutil.move(fichier_dxf, "/home/pi/Scan2dxf/DXF")
    shutil.move(fichier_svg, "/home/pi/Scan2dxf/SVG")
    shutil.move(fichier_pbm, "/home/pi/Scan2dxf/PBM")
    shutil.move(fichier_bmp, "/home/pi/Scan2dxf/BMP")
    shutil.move(fichier_pdf, "/home/pi/Scan2dxf/PDF")

    print ("Fichiers disponibles pour la découpe,ià récuperer dans le répertoire partagé Decoupfil")
    # Tout est terminé, on attend pour un autre scan
    print("Appuyez sur le bouton pour lancer un nouveau scan")
    OkLed.on()
    BusyLed.off()
    
# Définitions des fonctions d'appui court/long du bouton
def appui_long(bouton):
    bouton.was_held = True
    print ("Le bouton a été appuyé plus de 5s, on éteint le système")
    shutdown()

def bouton_relache(bouton):
    if not bouton.was_held:
        appui_court()
    bouton.was_held = False

def appui_court():
    print ("Appui court, on lance la procédure de scan")
    procedure_scan()
 
# initialisation de sane
print ("Recherche d'un scanner")
sane.init()
#boucle principale
while True:
    devices = sane.get_devices()
    if  (len(devices)==0) :
        print ("Aucun scanner n'a pu être détecté, vérifiez le branchement ou la compatibilité du matériel")
    else :   
        print ("Scanner disponible : ", devices)
        myscanner = sane.open(devices[0][0])
        myscanner.resolution = scan_resolution
        myscanner.contrast= scan_contraste
        myscanner.brightness= scan_luminosite
        myscanner.mode = 'Gray'
        sane.UNIT_STR=3
        myscanner.tl_y=11
        myscanner.tl_x=11
        myscanner.br_x=197
        myscanner.br_y=287
        print("Appuyez sur le bouton pour lancer le scan")
        OkLed.on()
        while True:
            bouton.when_held = appui_long
            bouton.when_released = appui_court

