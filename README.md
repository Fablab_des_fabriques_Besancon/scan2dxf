# Scan2dxf

Scan2 DXF est script Python permettant de scanner, traiter l'image et la vectoriser afin d'obtenir un fichier vectoriel exploitable pour une machine à commande numérique : découpe laser, découpe vinyle, découpe à fil chaud, brodeuse...

Utilisé sur un raspberry pi zero sur lequel on ajoute un circuit fait maison (https://framagit.org/Fablab_des_fabriques_Besancon/pcb_pizero_scan2dxf) il permet une solution autonome permettant la création de chemins à partir de découpages papier ou dessin au feutre par simple appui sur un bouton.

Dépendances :
Le script s'appuie sur python3-sane pour la gestion du scanner et python3-gpiozero pour la gestion du bouton et des leds, pour les installer :

    sudo apt-get install python3-sane python3-gpiozero
    
Il faut également ajouter l'user pi au groupe scanner via la commande :

    sudo usermod -a -G scanner pi

Aussi nous avons dû créer une règle udev pour que l'user pi puisse accéder au scanner  :

    echo 'ENV{DEVTYPE}=="usb_device", ENV{libsane_matched}=="yes", MODE="0666", GROUP="scanner"' | sudo tee /etc/udev/rules.d/55-libsane.rules

Le traitement de l'image et la vectorisation sont réalisés par potrace (http://potrace.sourceforge.net/) que l'on installe via la commande :

    sudo apt-get install potrace.
    
L'accès au fichier est possible à travers un partage réseau grâce à Samba (https://www.samba.org/) que l'on installe via la commande :

    sudo apt-get install samba

Le script sera automatiquement lancé au démarrage via un service systemd

Enfin, le raspberry pi zero est configuré pour générer son propre réseau WIFI ouvert (voir notre wiki https://wiki-fablab.grandbesancon.fr/doku.php?id=howto:raspberry-pi:wirelessaccespoint)

Utilisation :
Au démarrage, le service est lancé, si un scanner est trouvé la LED verte s'allume pour indiquer que tout est prêt.
Un appui court sur le bouton lance la procédure de scan, transforme le fichier avec mkbitmap puis effectue la vectorisation uax formats DXF et SVG.
Les fichiers sont disponibles à travers un partage de dossier.
Un appui long (minimum 5 secondes lance l'extinction du raspberry pi)

